# Copyright 2022 Akretion France (http://www.akretion.com/)
# @author: Alexis de Lattre <alexis.delattre@akretion.com>
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Product Analytic Sale",
    "version": "2.0.1.0.0",
    "category": "Accounting",
    "license": "AGPL-3",
    "summary": "Glue module between sale and product_analytic",
    "author": "Akretion,Odoo Community Association (OCA)",
    "maintainers": ["alexis-via"],
    "website": "https://gitlab.com/flectra-community/account-analytic",
    "depends": ["product_analytic", "sale"],
    "auto_install": True,
    "installable": True,
}
