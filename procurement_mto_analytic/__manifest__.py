# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Purchase Analytic (MTO)",
    "summary": "This module sets analytic account in purchase order line from "
    "sale order analytic account",
    "version": "2.0.1.0.0",
    "category": "Analytic",
    "license": "AGPL-3",
    "author": "Tecnativa, VentorTech, Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/account-analytic",
    "depends": ["sale_stock", "purchase_stock"],
    "installable": True,
}
