# Copyright (C) 2021 Open Source Integrators
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Carry Analytic Account to child Manufacturing Orders",
    "summary": "Carry the Manufacturing Order Analytic Account to the "
    "generated Manufacturing Orders",
    "version": "2.0.1.0.1",
    "author": "Open Source Integrators, Odoo Community Association (OCA)",
    "category": "Manufacturing",
    "website": "https://gitlab.com/flectra-community/account-analytic",
    "license": "AGPL-3",
    "depends": ["mrp_analytic"],
    "installable": True,
    "maintainer": "dreispt",
    "development_status": "Beta",
}
