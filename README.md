# Flectra Community / account-analytic

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[mrp_analytic_sale_project](mrp_analytic_sale_project/) | 2.0.1.0.0| Carry the Sales Order Analytic Account to the generated Manufacturing Orders
[account_analytic_tag_default](account_analytic_tag_default/) | 2.0.1.0.0| Set default tags on analytic accounts.
[pos_analytic_by_config](pos_analytic_by_config/) | 2.0.1.0.0| Use analytic account defined on POS configuration for POS orders
[analytic_base_department](analytic_base_department/) | 2.0.1.0.0| Add relationshet between Analytic and Department
[account_analytic_sequence](account_analytic_sequence/) | 2.0.1.0.1|         Restore the analytic account sequence
[sale_stock_analytic](sale_stock_analytic/) | 2.0.1.0.0|         Copies the analytic account of the sale order and the analytic tags        of the sale order line to the stock move
[analytic_activity_based_cost](analytic_activity_based_cost/) | 2.0.4.0.1| Assign overhead costs to activities, using Analytic Items
[product_analytic](product_analytic/) | 2.0.1.0.0| Add analytic account on products and product categories
[product_analytic_purchase](product_analytic_purchase/) | 2.0.1.0.1| Glue module between purchase and product_analytic
[account_analytic_required](account_analytic_required/) | 2.0.1.0.3| Account Analytic Required
[purchase_analytic](purchase_analytic/) | 2.0.2.0.0| Purchase Analytic
[product_analytic_sale](product_analytic_sale/) | 2.0.1.0.0| Glue module between sale and product_analytic
[mrp_analytic_child_mo](mrp_analytic_child_mo/) | 2.0.1.0.1| Carry the Manufacturing Order Analytic Account to the generated Manufacturing Orders
[stock_picking_analytic](stock_picking_analytic/) | 2.0.1.0.0|         Allows to define the analytic account on picking level
[analytic_partner_hr_timesheet](analytic_partner_hr_timesheet/) | 2.0.1.0.0| Classify HR activities by partner
[account_analytic_parent](account_analytic_parent/) | 2.0.1.0.1|         This module reintroduces the hierarchy to the analytic accounts.
[analytic_partner](analytic_partner/) | 2.0.1.0.0| Search and group analytic entries by partner
[purchase_request_analytic](purchase_request_analytic/) | 2.0.1.0.0| Purchase Request Analytic
[account_analytic_wip](account_analytic_wip/) | 2.0.3.0.2| Track and report WIP and Variances based on Analytic Items
[account_move_update_analytic](account_move_update_analytic/) | 2.0.1.0.1| This module allows the user to update analytic on posted moves
[stock_analytic](stock_analytic/) | 2.0.2.0.0| Adds an analytic account and analytic tags in stock move
[analytic_tag_dimension](analytic_tag_dimension/) | 2.0.1.0.0| Group Analytic Entries by Dimensions
[stock_inventory_analytic](stock_inventory_analytic/) | 2.0.1.0.2|         Stock Inventory Analytic 
[mrp_analytic](mrp_analytic/) | 2.0.1.1.0| Adds the analytic account to the production order
[account_analytic_distribution_required](account_analytic_distribution_required/) | 2.0.1.0.0| Account Analytic Distribution Required
[analytic_tag_dimension_enhanced](analytic_tag_dimension_enhanced/) | 2.0.1.1.2| Analytic Accounts Dimensions Enhanced
[purchase_stock_analytic](purchase_stock_analytic/) | 2.0.1.0.0|         Copies the analytic account of the purchase order item to the stock move
[procurement_mto_analytic](procurement_mto_analytic/) | 2.0.1.0.0| This module sets analytic account in purchase order line from sale order analytic account


