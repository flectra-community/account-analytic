# Copyright 2020 ACSONE SA/NV
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    "name": "Sale Stock Analytic",
    "summary": """
        Copies the analytic account of the sale order and the analytic tags
        of the sale order line to the stock move""",
    "version": "2.0.1.0.0",
    "license": "AGPL-3",
    "author": "ACSONE SA/NV,Odoo Community Association (OCA)",
    "website": "https://gitlab.com/flectra-community/account-analytic",
    "depends": ["sale_stock", "stock_analytic"],
}
