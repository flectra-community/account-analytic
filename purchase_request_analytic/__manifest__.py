# Copyright 2019 Eficent Business and IT Consulting Services S.L.
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl.html).

{
    "name": "Purchase Request Analytic",
    "version": "2.0.1.0.0",
    "author": "ForgeFlow, Odoo Community Association (OCA)",
    "category": "Purchase Management",
    "website": "https://gitlab.com/flectra-community/account-analytic",
    "depends": ["purchase_request"],
    "data": ["views/purchase_request_views.xml"],
    "license": "AGPL-3",
    "installable": True,
}
